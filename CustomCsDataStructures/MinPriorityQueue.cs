﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomCsDataStructures
{
    public class MinPriorityQueue<TPriority, TValue>
    {
        private List<KeyValuePair<TPriority, TValue>> _heap;
        private IComparer<TPriority> _comparer;

        /// <summary> Gets the number of elements in the priority queue </summary>
        public int Count { get { return _heap.Count; } } // Count Property

        /// <summary> Constructor for use when comparer for priorites is already defined </summary>
        public MinPriorityQueue()
            : this(Comparer<TPriority>.Default)
        { }

        /// <summary>
        /// Constructor for use when you want to use a custom comparer
        /// </summary>
        /// <param name="comparer"></param>
        public MinPriorityQueue(IComparer<TPriority> comparer)
        {
            if (comparer == null)
                throw new ArgumentNullException();
            _heap = new List<KeyValuePair<TPriority, TValue>>();
            _comparer = comparer;
        }

        /// <summary>
        /// Adds an object to the prioriy queue and reorders the container so that the highest priority is first
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="value"></param>
        public void Enqueue(TPriority priority, TValue value)
        {
            // create key value pair, then insert at "end" of our heap
            KeyValuePair<TPriority, TValue> kp = new KeyValuePair<TPriority, TValue>(priority, value);
            _heap.Add(kp);

            // After insert, UpHeap
            HeapUp(_heap.Count - 1);
        }

        /// <summary>
        /// Performs a heap up operation on the underlying container. Used after an object is inserted into the queue.
        /// </summary>
        /// <param name="pos"></param>
        private void HeapUp(int pos)
        {
            if (pos >= _heap.Count)
                return;
            // a node in the heap, heap[i], has its parent at heap[(i-1)/2)] .... well technically floor(heap[(i-1)/2])
            // a node in the heap, heap[i], has its children at heap[2*i+1] and heap[2*i+2]
            while (pos > 0)
            {
                int parent = (pos - 1) / 2;

                // ICompare.Compare(lhs, rhs) returns: 
                //                                      <0 if lhs < rhs;
                //                                       0 if lhs == rhs;
                //                                      >0 if lhs > rhs
                // therefore the line below is equivalent to: "if (parentKeyVal > childKeyVal)"
                if (_comparer.Compare(_heap[parent].Key, _heap[pos].Key) > 0)
                {
                    // if child has a higher priority (lower key value) than the parent, switch them
                    Switch(pos, parent);
                    pos = parent;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Removes and returns the object in the PriorityQueue with the highest priority.
        /// </summary>
        /// <returns></returns>
        public TValue Dequeue()
        {
            if (_heap.Count != 0) // if queue contains objects
            {
                TValue result = _heap[0].Value;
                HeapDown();
                return result;
            }
            else // if queue is empty throw exception
                throw new InvalidOperationException("Priority queue is empty");
        }

        /// <summary>
        /// Performs a HeapDown operation. Used after removing objects from the queue.
        /// </summary>
        private void HeapDown()
        {
            if (_heap.Count <= 1)
            {
                _heap.Clear();
                return;
            }

            // replace root with last element
            _heap[0] = _heap[_heap.Count - 1];
            _heap.RemoveAt(_heap.Count - 1);

            int current = 0; // start HeapDown from the root

            while (true)
            {
                // a node in the, heap[i], has its parent at heap[(i-1)/2)] .... well technically floor(heap[(i-1)/2])
                // a node in the, heap[i], has its children at heap[2*i+1] and heap[2*i+2]
                int leftChild = 2 * current + 1;
                int rightChild = 2 * current + 2;
                int smallest = current; // smallest starts at our currently examined node

                // ICompare.Compare(lhs, rhs) returns: 
                //                                      <0 if lhs < rhs;
                //                                       0 if lhs == rhs;
                //                                      >0 if lhs > rhs

                // REMEMBER THAT A SMALLER KEY VALUE = A HIGHER PRIORITY

                if (leftChild < _heap.Count && _comparer.Compare(_heap[leftChild].Key, _heap[smallest].Key) < 0) // if leftChild < smallest, set smallest to leftChild
                    smallest = leftChild;

                if (rightChild < _heap.Count && _comparer.Compare(_heap[rightChild].Key, _heap[smallest].Key) < 0) // if rightChild < smallest, set smallest to rightChild
                    smallest = rightChild;

                if (current != smallest) // if node has child with a smaller key (higher priority), switch node with smallest child
                {
                    Switch(current, smallest);
                    current = smallest;
                }
                else // if the current parent has a higher priority than both its children our heap is in the correct shape
                {
                    return;
                }
            }

        }


        /// <summary>
        /// Returns object at front of queue without removing it
        /// </summary>
        public TValue Peek()
        {
            if (Count > 0)
            {
                Stack<int> t = new Stack<int>();
                return _heap[0].Value;
            }
            else
            {
                throw new InvalidOperationException("Priority queue is empty");
            }
        }

        /// <summary>
        /// Determines whether an element is in the  Priority Queue.
        /// </summary>
        /// <param name="v">The Object to locate in the Priority Queue.</param>
        /// <returns></returns>
        public bool Contains(TValue v)
        {
            foreach (var item in _heap)
            {
                if (item.Value.Equals(v))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Switches the elements located at pos and pos2. Used in HeapUp operations
        /// </summary>
        /// <param name="pos">Destination of pos2</param>
        /// <param name="pos2">Destination of pos</param>
        private void Switch(int pos, int pos2)
        {
            KeyValuePair<TPriority, TValue> temp = _heap[pos];
            _heap[pos] = _heap[pos2];
            _heap[pos2] = temp;
        }
    } // class
}
