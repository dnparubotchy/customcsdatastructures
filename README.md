## C\# Data Structures

During the AI course I took as part of my degree I was developing something that used A-Star in C\# and I discovered that C\# didn't have a priority queue data structure in the standard library so I decided to make one.

Now I've got some time for projects I am going to implement some other data structures that I think might be useful in the future such as Tries (https://en.wikipedia.org/wiki/Trie)